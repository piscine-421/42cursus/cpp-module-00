/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42quebec>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/05 15:50:21 by lcouturi          #+#    #+#             */
/*   Updated: 2024/03/19 10:37:35 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

int	main(int argc, char **argv)
{
	std::string	arg;

	if (argc == 1)
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
	else
	{
		for (int i = 1; i < argc; i++)
		{
			arg = argv[i];
			for (unsigned int j = 0; j < arg.length(); j++)
				std::cout << (char)std::toupper(arg[j]);
		}
	}
	std::cout << std::endl;
	return (EXIT_SUCCESS);
}
