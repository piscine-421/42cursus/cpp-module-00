// ************************************************************************** //
//                                                                            //
//                Account.cpp for GlobalBanksters United                      //
//                Created on  : Thu Nov 20 19:43:15 1989                      //
//                Last update : Wed Jan 04 14:54:06 1992                      //
//                Made by : Léopold Couturier-Otis <lco@gbu.com>              //
//                                                                            //
// ************************************************************************** //

#include "Account.hpp"
#include <iostream>
#include <time.h>

int	Account::getNbAccounts(void)
{
	return (Account::_nbAccounts);
}

int	Account::getTotalAmount(void)
{
	return (Account::_totalAmount);
}

int	Account::getNbDeposits(void)
{
	return (Account::_totalNbDeposits);
}

int	Account::getNbWithdrawals(void)
{
	return (Account::_totalNbWithdrawals);
}

void	Account::displayAccountsInfos(void)
{
	Account::_displayTimestamp();
	std::cout << "accounts:" << getNbAccounts() << ";total:" << getTotalAmount() << ";deposits:" << getNbDeposits() << ";withdrawals:" << getNbWithdrawals() << std::endl;
	return ;
}

Account::Account(int initial_deposit)
{
	Account::_accountIndex = _nbAccounts;
	Account::_amount = initial_deposit;
	Account::_displayTimestamp();
	std::cout << "index:" << Account::_accountIndex << ";amount:" << Account::_amount << ";created\n";
	Account::_nbAccounts++;
	Account::_totalAmount += initial_deposit;
}

Account::~Account(void)
{
	Account::_displayTimestamp();
	std::cout << "index:" << Account::_accountIndex << ";amount:" << Account::_amount << ";closed\n";
}

void	Account::makeDeposit(int deposit)
{
	Account::_totalAmount += deposit;
	Account::_nbDeposits++;
	Account::_totalNbDeposits++;
	Account::_displayTimestamp();
	std::cout << "index:" << Account::_accountIndex << ";p_amount:" << Account::_amount << ";deposit:" << deposit << ";amount:";
	Account::_amount += deposit;
	std::cout << Account::_amount << ";nb_deposits:" << Account::_nbDeposits << std::endl;
}

bool	Account::makeWithdrawal(int withdrawal)
{
	Account::_displayTimestamp();
	std::cout << "index:" << Account::_accountIndex << ";p_amount:" << Account::_amount << ";withdrawal:";
	if (withdrawal > Account::_amount)
	{
		std::cout << "refused\n";
		return (false);
	}
	Account::_amount -= withdrawal;
	Account::_totalAmount -= withdrawal;
	Account::_nbWithdrawals++;
	Account::_totalNbWithdrawals++;
	std::cout << withdrawal << ";amount:" << Account::_amount << ";nb_withdrawals:" << Account::_nbWithdrawals << std::endl;
	return (true);
}

void	Account::displayStatus(void) const
{
	Account::_displayTimestamp();
	std::cout << "index:" << Account::_accountIndex << ";amount:" << Account::_amount << ";deposits:" << Account::_nbDeposits << ";withdrawals:" << Account::_nbWithdrawals << std::endl;
}

int		Account::_nbAccounts = 0;
int		Account::_totalAmount = 0;
int		Account::_totalNbDeposits = 0;
int		Account::_totalNbWithdrawals = 0;

void	Account::_displayTimestamp(void)
{
	time_t	raw_time;
	tm		*real_time;

	time(&raw_time);
	real_time = localtime(&raw_time);
	std::cout << '[' << real_time->tm_year + 1900;
	if (real_time->tm_mon < 10)
		std::cout << 0;
	std::cout << real_time->tm_mon;
	if (real_time->tm_mday < 10)
		std::cout << 0;
	std::cout << real_time->tm_mday << '_';
	if (real_time->tm_hour < 10)
		std::cout << 0;
	std::cout << real_time->tm_hour;
	if (real_time->tm_min < 10)
		std::cout << 0;
	std::cout << real_time->tm_min;
	if (real_time->tm_sec < 10)
		std::cout << 0;
	std::cout << real_time->tm_sec << "] ";
}
