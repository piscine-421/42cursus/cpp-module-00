#include "PhoneBook.hpp"

std::string	PhoneBook::get_darkest_secret(int i) const
{
	return (contact[i % 8].get_darkest_secret());
}

std::string	PhoneBook::get_first_name(int i) const
{
	return (contact[i % 8].get_first_name());
}

std::string	PhoneBook::get_last_name(int i) const
{
	return (contact[i % 8].get_last_name());
}

std::string	PhoneBook::get_nickname(int i) const
{
	return (contact[i % 8].get_nickname());
}

std::string	PhoneBook::get_phone_number(int i) const
{
	return (contact[i % 8].get_phone_number());
}

void		PhoneBook::set_darkest_secret(int i, const std::string value)
{
	contact[i % 8].set_darkest_secret(value);
}

void		PhoneBook::set_first_name(int i, const std::string value)
{
	contact[i % 8].set_first_name(value);
}

void		PhoneBook::set_last_name(int i, const std::string value)
{
	contact[i % 8].set_last_name(value);
}

void		PhoneBook::set_nickname(int i, const std::string value)
{
	contact[i % 8].set_nickname(value);
}

void		PhoneBook::set_phone_number(int i, const std::string value)
{
	contact[i % 8].set_phone_number(value);
}

