#include "Contact.hpp"

std::string	Contact::get_darkest_secret(void) const
{
	return (darkest_secret);
}

std::string	Contact::get_first_name(void) const
{
	return (first_name);
}

std::string	Contact::get_last_name(void) const
{
	return (last_name);
}

std::string	Contact::get_nickname(void) const
{
	return (nickname);
}

std::string	Contact::get_phone_number(void) const
{
	return (phone_number);
}

void		Contact::set_darkest_secret(const std::string value)
{
	darkest_secret = value;
}

void		Contact::set_first_name(const std::string value)
{
	first_name = value;
}

void		Contact::set_last_name(const std::string value)
{
	last_name = value;
}

void		Contact::set_nickname(const std::string value)
{
	nickname = value;
}

void		Contact::set_phone_number(const std::string value)
{
	phone_number = value;
}
