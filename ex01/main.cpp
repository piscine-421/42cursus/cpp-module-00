/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phonebook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42quebec>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/05 15:50:21 by lcouturi          #+#    #+#             */
/*   Updated: 2024/03/19 10:37:35 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "PhoneBook.hpp"

std::string	prompt(std::string prompt)
{
	std::string	line;

	std::cout << "Enter " << prompt << "." << std::endl;
	std::getline(std::cin, line);
	if (std::cin.eof())
		exit(EXIT_FAILURE);
	while (line == "")
	{
		std::getline(std::cin, line);
		if (std::cin.eof())
			exit(EXIT_FAILURE);
	}
	return (line);
}

void		print_cell(std::string input)
{
	std::string	line;

	line = input;
	if (line.length() > 10)
		line.replace(9, line.length() - 9, ".");
	while (line.size() < 10)
		line.insert(0, " ");
	std::cout << '|' << line;
}

void		print_row(std::string a, std::string b, std::string c, std::string d)
{
	print_cell(a);
	print_cell(b);
	print_cell(c);
	print_cell(d);
	std::cout << '|' << std::endl;
}

int			main(void)
{
	int			i;
	int			j;
	std::string	line;
	PhoneBook	p;

	i = 0;
	while (1)
	{
		std::getline(std::cin, line);
		if (std::cin.eof())
			return (EXIT_FAILURE);
		else if (line == "ADD")
		{
			p.set_first_name(i, prompt("first name"));
			p.set_last_name(i, prompt("last name"));
			p.set_nickname(i, prompt("nickname"));
			p.set_phone_number(i, prompt("phone number"));
			p.set_darkest_secret(i, prompt("darkest secret"));
			i++;
		}
		else if (i && line == "SEARCH")
		{
			print_row("INDEX", "FIRST NAME", "LAST NAME", "NICKNAME");
			for (j = 0; j < i && j < 8; j++)
				print_row(std::to_string(j + 1), p.get_first_name(j), p.get_last_name(j), p.get_nickname(j));
			std::cout << "Enter index." << std::endl;
			while (1)
			{
				std::getline(std::cin, line);
				if (std::cin.eof())
					return (EXIT_FAILURE);
				while (!isdigit(line[0]))
				{
					std::getline(std::cin, line);
					if (std::cin.eof())
						return (EXIT_FAILURE);
				}
				j = std::stoi(line) - 1;
				if (j >= 0 && j < i && j < 8)
					break ;
			}
			std::cout << "First name:     " << p.get_first_name(j) << std::endl;
			std::cout << "Last name:      " << p.get_last_name(j) << std::endl;
			std::cout << "Nickname:       " << p.get_nickname(j) << std::endl;
			std::cout << "Phone number:   " << p.get_phone_number(j) << std::endl;
			std::cout << "Darkest secret: " << p.get_darkest_secret(j) << std::endl;
		}
		else if (line == "EXIT")
			return (EXIT_SUCCESS);
	}
}
