#include "Contact.hpp"

class	PhoneBook
{
	public:
		std::string	get_darkest_secret(int i) const;
		std::string	get_first_name(int i) const;
		std::string	get_last_name(int i) const;
		std::string	get_nickname(int i) const;
		std::string	get_phone_number(int i) const;
		void		set_darkest_secret(int i, const std::string value);
		void		set_first_name(int i, const std::string value);
		void		set_last_name(int i, const std::string value);
		void		set_nickname(int i, const std::string value);
		void		set_phone_number(int i, const std::string value);
	private:
		Contact		contact[8];
};
