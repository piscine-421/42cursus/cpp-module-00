#include <string>

class	Contact
{
	public:
		std::string	get_darkest_secret(void) const;
		std::string	get_first_name(void) const;
		std::string	get_last_name(void) const;
		std::string	get_nickname(void) const;
		std::string	get_phone_number(void) const;
		void		set_darkest_secret(const std::string value);
		void		set_first_name(const std::string value);
		void		set_last_name(const std::string value);
		void		set_nickname(const std::string value);
		void		set_phone_number(const std::string value);
	private:
		std::string	darkest_secret;
		std::string	first_name;
		std::string	last_name;
		std::string	nickname;
		std::string	phone_number;
};
